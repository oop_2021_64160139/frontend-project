import type User from "@/types/Users";
import http from "./axios";
function getUsers() {
  return http.get("/users");
}

function saveUsers(user: User) {
  return http.post("/users", user);
}
function UpdateUsers(id: number, user: User) {
  return http.patch(`/users/${id}`, user);
}
function DeleteUsers(id: number) {
  return http.delete(`/users/${id}`);
}
export default { getUsers, saveUsers, UpdateUsers, DeleteUsers };
