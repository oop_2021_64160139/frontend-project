export default interface User {
  id?: number;

  name: string;

  tel: string;

  age: number;

  gender: string;

  createAt?: Date;

  updateAt?: Date;

  deleteAt?: Date;
}
