import { ref, computed, watch } from "vue";
import { defineStore } from "pinia";
import type Product from "@/types/Product";
import productSerVices from "@/services/product";
import { useLoadingStore } from "./loading";
import { useMessageStore } from "./message";

export const useProductStore = defineStore("Product", () => {
  const products = ref<Product[]>([]);
  const loadingStore = useLoadingStore();
  const messageStore = useMessageStore();
  const editedProduct = ref<Product>({ name: "", price: 0 });
  const dialog = ref(false);
  watch(dialog, (newDialog, oldDialog) => {
    if (!newDialog) {
      editedProduct.value = { name: "", price: 0 };
    }
  });
  async function getProduct() {
    loadingStore.isLoading = true;
    try {
      const res = await productSerVices.getProduct();
      products.value = res.data;
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถดึงข้อมูล product ได้");
    }
    loadingStore.isLoading = false;
  }

  async function saveProduct() {
    loadingStore.isLoading = true;
    try {
      if (editedProduct.value.id) {
        const res = await productSerVices.UpdateProduct(
          editedProduct.value.id,
          editedProduct.value
        );
      } else {
        const res = await productSerVices.saveProduct(editedProduct.value);
      }

      dialog.value = false;

      await getProduct();
    } catch (e) {
      messageStore.showError("ไม่สามารถบันทึก product ได้");
      console.log(e);
    }
    loadingStore.isLoading = false;
  }

  function editProduct(product: Product) {
    editedProduct.value = JSON.parse(JSON.stringify(product));
    dialog.value = true;
  }

  async function deleteProduct(id: number) {
    loadingStore.isLoading = true;
    try {
      const res = await productSerVices.DeleteProduct(id);
      await getProduct();
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถลบข้อมูล product ได้");
    }
    loadingStore.isLoading = false;
  }
  return {
    products,
    getProduct,
    dialog,
    editedProduct,
    saveProduct,
    editProduct,
    deleteProduct,
  };
});
